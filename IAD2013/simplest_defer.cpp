#include "simplest_defer.h"

using namespace std;

defer::defer(std::function<void()> toDefer)
	: m_deferred(move(toDefer))
{

}

defer::~defer()
{
	m_deferred();
}