#include "gtest/gtest.h"
#include <ppl.h>
#include <ppltasks.h>
#include <iostream>

using namespace std;
using namespace Concurrency;

void SimulateLongOperation(int cycles, char id)
{
	for (int i = 0; i < cycles; ++i)
	{
		cout << id;
	}
}

TEST(IAD2013Tests, PPL_BlockingReadFile)
{
	auto readingFn = []() {
		SimulateLongOperation(1000, 'f');
		return "Lorem ipsum dolor sit amet, consectetur adipiscing elit...";
	};

	auto decorateFn = [](const string& st) -> string {
		SimulateLongOperation(200, 'd');
		return "<html><body>" + st + "</body></html>";
	};

	// queste tre di seguito sono seriali e bloccanti
	SimulateLongOperation(500, '.'); 
	cout << endl << "Content: " << decorateFn(readingFn());
	SimulateLongOperation(500, ':');

	cout << endl;
}

TEST(IAD2013Tests, PPL_AsyncReadFileAndBlockingDecoration)
{
	auto readingTask = create_task([]() -> string {
		SimulateLongOperation(1000, 'f');
		return "Lorem ipsum dolor sit amet, consectetur adipiscing elit...";
	});

	auto decorateFn = [](const string& st) -> string {
		SimulateLongOperation(200, 'd');
		return "<html><body>" + st + "</body></html>";
	};

	SimulateLongOperation(500, '.'); // questa in parallelo alla lettura

	auto content = readingTask.get(); // bloccante
	auto decoratedContent = decorateFn(content); // bloccante

	SimulateLongOperation(500, ':');  
	cout << endl << "Content: " << decoratedContent << endl << endl;
}

TEST(IAD2013Tests, PPL_AsyncReadFileAndAsyncDecoration)
{
	auto decorateFn = [](const string& st) -> string {
		SimulateLongOperation(200, 'd');
		return "<html><body>" + st + "</body></html>";
	};

	auto readingTask = create_task([]() -> string {
		SimulateLongOperation(800, 'f');
		return "Lorem ipsum dolor sit amet, consectetur adipiscing elit...";
	}).then(decorateFn);

	// quelle prima e questa sono tutte in parallelo
	SimulateLongOperation(1200, '.'); 

	auto decoratedContent = readingTask.get(); // qui blocco
	cout << endl << "Content: " << decoratedContent << endl << endl;
}
