#include <iostream>
#include <thread>
#include <future>
#include <numeric>
#include <algorithm>
#include <vector>
#include <deque>

using namespace std;

// esempio bonus (task)

mutex taskMutex;
deque<packaged_task <int()>> tasks;
int taskCount = 3;

void execute_all_tasks()
{
	while (taskCount > 0)
	{
		packaged_task<int()> task;
		{
			lock_guard<mutex> l{ taskMutex };
			if (tasks.empty())
				continue;
			task = move(tasks.front());
			tasks.pop_front();
		}
		task();
		--taskCount;
	}
}

template<typename Fun>
future<int> add_task(Fun fun)
{
	packaged_task < int() > task(fun);
	auto f = task.get_future();
	lock_guard<mutex> l{ taskMutex };
	tasks.push_back(move(task));
	return f;
}

// some tasks
int SimpleComputation()
{
	this_thread::sleep_for(chrono::milliseconds(100));
	cout << "simple computation done..." << endl;
	return 10;
}

int HugeComputation()
{
	this_thread::sleep_for(chrono::milliseconds(300));
	cout << "huge computation done..." << endl;
	return 100;
}

void task_snippet()
{
	thread taskRunner { execute_all_tasks };

	
	auto f1 = add_task(HugeComputation);
	auto f2 = add_task(SimpleComputation);
	auto f3 = add_task(SimpleComputation);

	this_thread::sleep_for(chrono::milliseconds(500));

	cout << "res of f1 = " << f1.get() << endl;
	cout << "res of f2 = " << f2.get() << endl;
	cout << "res of f3 = " << f3.get() << endl;
	

	taskRunner.join(); 
}