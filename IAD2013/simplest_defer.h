#pragma once

#include <functional>

class defer
{
public:
	defer(std::function<void()> toDefer);
	~defer();

private:
	std::function<void()> m_deferred;
};