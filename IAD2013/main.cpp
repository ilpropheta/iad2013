// gmock
#include "gmock\gmock.h"
// STD Concurrency
#include <thread>
#include <future>
// STL
#include <iostream>
#include <numeric>
#include <algorithm>
#include <vector>
// windows
#include <Windows.h>
// my stuff
#include "simplest_defer.h"
#include "timer.h"

using namespace std;
using namespace Concurrency;
using namespace testing;

/// useful functions ///////////////////////////

vector<int> CreateVectorWithSize(size_t n)
{
	vector<int> vec(n);
	iota(begin(vec), end(vec), 0); // 0 1 2 3 4 ...
	return vec;
}

int CalculateAverage(const vector<int>& vec)
{
	return accumulate(begin(vec), end(vec), 0) / vec.size();
}

////////////////////////////////////////////////

// esempio 0
TEST(IAD2013Tests, DeferTest)
{
	int willBeOne = 0;
	try
	{
		defer d { [&willBeOne]{
			willBeOne = 1;
			cout << "defer_example end..." << endl;
		} };

		throw 1;
	}
	catch (...){} // solo per non propagare l'eccezione

	EXPECT_THAT(willBeOne, testing::Eq(1));
}

// esempio 1
TEST(IAD2013Tests, ThreadTest)
{
	auto vec = CreateVectorWithSize(100000);
	int average = 0;

	thread t1 { [&] {
		average = CalculateAverage(vec);
	} };

	auto maxElement = *max_element(begin(vec), end(vec));
	t1.join();

	cout << "max is => " << maxElement << endl;
	cout << "average is => " << average << endl;

	EXPECT_THAT(maxElement, Eq(99999)); 
	EXPECT_THAT(average, Eq(7049));
}

// esempio 2
TEST(IAD2013Tests, FutureTest)
{
	auto vec = CreateVectorWithSize(100000);
	auto willBeAvg = async([&]{
		return CalculateAverage(vec);
	});

	auto maxElement = *max_element(begin(vec), end(vec));
	cout << "max is => " << maxElement << endl;
	auto average = willBeAvg.get();
	cout << "average is => " << average << endl;

	EXPECT_THAT(maxElement, Eq(99999));
	EXPECT_THAT(average, Eq(7049));
}


// esempio 3
void CalculateAverageWithPromise(const vector<int>& vec, promise<int>& p)
{
	p.set_value(CalculateAverage(vec));
}

TEST(IAD2013Tests, PromiseTest)
{
	auto vec = CreateVectorWithSize(100000);
	promise<int> willSetTheAvg;
	auto willBeAvg = willSetTheAvg.get_future();

	thread t1 { CalculateAverageWithPromise, ref(vec), ref(willSetTheAvg) };

	auto maxElement = *max_element(begin(vec), end(vec));
	cout << "max is => " << maxElement << endl;
	auto average = willBeAvg.get();
	cout << "average is => " << average << endl;

	t1.join();

	EXPECT_THAT(maxElement, Eq(99999));
	EXPECT_THAT(average, Eq(7049));
}

int main(int argc, char* argv[])
{
	::testing::InitGoogleMock(&argc, argv);
	return RUN_ALL_TESTS();
}