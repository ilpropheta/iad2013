#pragma once

#include <thread>

class scoped_thread
{
public:
	template<typename... Args>
	scoped_thread(Args&&... args)
		: m_thread{ std::forward<Args>(args)... }
	{

	}

	~scoped_thread()
	{
		if (m_thread.joinable())
			m_thread.join();
	}
private:
	std::thread m_thread;
};

	