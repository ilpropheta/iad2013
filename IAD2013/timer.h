#pragma once

#include <chrono>

class timer
{
public:
	timer();

	__int64 elapsed_millis();
	void reset();

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
};