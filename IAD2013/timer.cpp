#include "timer.h"

using namespace std;
using namespace std::chrono;

timer::timer()
{
	start = high_resolution_clock::now();
}

__int64 timer::elapsed_millis()
{
	auto stop = high_resolution_clock::now();
	return duration_cast<milliseconds>(duration<double>(stop - start)).count();
}

void timer::reset()
{
	start = high_resolution_clock::now();
}
